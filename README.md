# OpenML dataset: health_insurance

https://www.openml.org/d/44955

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Data Description**

Dataset is a cross-section study from 1993 in United States.

It presents dataset about health insurance and hours worked by wives. Each instance is a data about a married woman.

**Attribute Description**

1. *whrswk* - hours worked per week by wife, target feature
2. *hhi* - whether wife covered by husband's health insurance
3. *whi* - whether wife has health insurance through her job ?
4. *hhi2* - whether husband has health insurance through her job ?
5. *education* - a factor with levels, "<9years", "9-11years", "12years", "13-15years", "16years", ">16years"
6. *race* - "white", "black", "other"
7. *hispanic* - "yes" or "no"
8". *experience* - years of potential work experience
9. *kidslt6* - number of kids under age of 6
10. *kids618* - number of kids 6-18 years old
11. *husby* - husband's income in thousands of dollars
12. *region* - one of "other", "northcentral", "south", "west"
13. *wght* - sampling weight (should be ignored)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44955) of an [OpenML dataset](https://www.openml.org/d/44955). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44955/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44955/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44955/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

